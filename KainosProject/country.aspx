﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="country.aspx.cs" Inherits="KainosProject.country" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Countries and Languages</title>
    <link rel="Stylesheet" href="~/style.css" type="text/css" />   
</head>
<body style="background-color: #B9CBFF">
    <form id="form1" runat="server">
    <div>
        <h1 class="auto-style1">Language use in <%= countryName %></h1>      
    </div>
    <div style="text-align: center; height: 30px;"> 
        <a runat="server" href="/countries" class="auto-style2">Countries</a>
        <a runat="server" href="/groupedCountries" class="auto-style2">Languages by Numbers</a>
    </div>
    <div style="text-align: center">
        <asp:Chart ID="Chart1" runat="server" Height="377px" style="text-align: center" Width="399px">
            <series>
                <asp:Series ChartType="Pie" Name="PercSeries" IsValueShownAsLabel="true" Legend="Legend1">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        <Legends><asp:Legend Name="Legend1"></asp:Legend></Legends></asp:Chart>
    </div>
     <div>
        <h3 class="auto-style3">Author: Błażej Galiński</h3>
    </div>
    </form>
</body>
</html>
