﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using Npgsql;

namespace KainosProject
{
    public partial class countries : System.Web.UI.Page
    {
        NpgsqlDataAdapter da;
        NpgsqlCommand cmd = new NpgsqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        // Uzupełnienie strony treścią
        public void BindData()
        {
            DataTable dt = new DataTable(); // DataTable z wynikiem zapytania SQL
            HyperLinkField linkField = new HyperLinkField();

            string connstring = String.Format("Server=ec2-54-225-101-124.compute-1.amazonaws.com;Port=5432;" +
                    "User Id=mmmwsvrkwfstit;Password=vlliFqilBOUVyWVY-FZfV50Wa3;Database=ddehqcgsacci3g;sslmode=require");

            // Tworzenie połączenia
            NpgsqlConnection connection = new NpgsqlConnection(connstring);

            try
            {
                // komenda SQL
                cmd.CommandText = "SELECT continent AS \"Continent\", name AS \"Country\", language AS \"Language\", population AS \"Population\", ROUND(CAST(percentage as numeric), 2) AS \"Official Language Use %\", FLOOR(percentage) AS \"Official Language Population Use\", country.code AS \"Code\"" +
                                    "FROM countryLanguage JOIN country ON countryLanguage.countrycode = country.code WHERE isofficial=true ORDER BY continent ASC, name ASC, percentage DESC";

                cmd.Connection = connection;
                da = new NpgsqlDataAdapter(cmd);
                da.Fill(dt);

                // dodawanie kolumn pojedynczo z DataTable do GridView
                foreach (DataColumn col in dt.Columns)
                {
                    BoundField bfield = new BoundField();
                    bfield.DataField = col.ColumnName;
                    bfield.HeaderText = col.ColumnName;
                    GridView1.Columns.Add(bfield);
                }

                GridView1.AutoGenerateColumns = false;
                GridView1.DataSource = dt;
                GridView1.Columns[6].Visible = false; // ukrycie kolumny z CountryCode
                GridView1.DataBind();

                // Dodanie kolumny z "Details"
                foreach (GridViewRow row in GridView1.Rows)
                {
                    linkField.HeaderText = "&nbsp;";
                    linkField.Text = "Details";
                    linkField.DataNavigateUrlFields = new string[] { "Code" };
                    linkField.DataNavigateUrlFormatString = "~/country.aspx/{0}";

                }
                GridView1.Columns.Insert(6, linkField); // wstawienie nowej kolumny na odpowiednim miejscu
                GridView1.DataBind();

                connection.Close();

            }
            catch (Exception)
            {
                connection.Close();
            }
        }
    }
}