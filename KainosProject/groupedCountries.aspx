﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="groupedCountries.aspx.cs" Inherits="KainosProject.groupedCountries" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Countries and Languages</title>
    <link rel="Stylesheet" href="~/style.css" type="text/css" />
</head>
<body style="background-color: #B9CBFF">
    <form id="form1" runat="server">
    <div>
        <h1 class="auto-style1">Global Language use</h1>
    </div>
    <div style="text-align: center; height: 30px;"> 
        <a runat="server" href="/countries" class="auto-style2">Countries</a>
        <a runat="server" href="/groupedCountries" class="auto-style2">Languages by Numbers</a>
    </div>
    <div style="text-align: center">
    
        <asp:GridView ID="GridView1" runat="server" style="text-align: left" HorizontalAlign="Center" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#002876" />
        </asp:GridView>
    
    </div>
    <div>
        <h3 class="auto-style3">Author: Błażej Galiński</h3>
    </div>
    </form>
</body>
</html>
