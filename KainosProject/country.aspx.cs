﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using Npgsql;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace KainosProject
{
    public partial class country : System.Web.UI.Page
    {
        NpgsqlDataAdapter da;
        NpgsqlCommand cmd = new NpgsqlCommand();

        public string countryCode { get; set; }
        public string countryName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                countryCode = url.GetLast(3);
                BindData();
            }
        }

        // Uzupełnienie strony treścią
        public void BindData()
        {
            DataTable dt_code = new DataTable(); // Tabela z CountryCode
            DataTable dt_perc = new DataTable(); // Tabela z CountryName i Percentage

            string connstring = String.Format("Server=ec2-54-225-101-124.compute-1.amazonaws.com;Port=5432;" +
                    "User Id=mmmwsvrkwfstit;Password=vlliFqilBOUVyWVY-FZfV50Wa3;Database=ddehqcgsacci3g;sslmode=require");

            // Tworzenie połączenia
            NpgsqlConnection connection = new NpgsqlConnection(connstring);

            try
            {
                // komenda SQL
                cmd.CommandText = "SELECT name FROM country WHERE country.code=\'" + countryCode + "\'";

                cmd.Connection = connection;
                da = new NpgsqlDataAdapter(cmd);
                da.Fill(dt_code);
                connection.Open();

                // Zapis nazwy kraju
                foreach (DataRow row in dt_code.Rows)
                {
                    countryName = row["name"].ToString();
                }

                // komenda SQL
                cmd.CommandText = "SELECT language, percentage FROM countryLanguage WHERE countryLanguage.countrycode=\'" + countryCode + "\'";

                da = new NpgsqlDataAdapter(cmd);
                da.Fill(dt_perc);

                //Utworzenie pie chart
                createPieChart(dt_perc);

                connection.Close();

            }
            catch (Exception)
            {
                connection.Close();
            }
        }

        // Tworzenie PieChart
        void createPieChart(DataTable dtable)
        {
            Chart1.Series["PercSeries"].XValueMember = "language";
            Chart1.Series["PercSeries"].YValueMembers = "percentage";
            Chart1.DataSource = dtable;
            Chart1.DataBind();
        }
    }

    //Rozszerzenie string o uzyskanie ostatnich n znaków
    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
    }
}