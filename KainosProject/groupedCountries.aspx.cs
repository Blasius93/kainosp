﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using Npgsql;

namespace KainosProject
{
    public partial class groupedCountries : System.Web.UI.Page
    {
        NpgsqlDataAdapter da;
        NpgsqlCommand cmd = new NpgsqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            DataSet ds = new DataSet(); // DataSet z wynikiem zapytania SQL

            string connstring = String.Format("Server=ec2-54-225-101-124.compute-1.amazonaws.com;Port=5432;" +
                    "User Id=mmmwsvrkwfstit;Password=vlliFqilBOUVyWVY-FZfV50Wa3;Database=ddehqcgsacci3g;sslmode=require");

            // Tworzenie połączenia
            NpgsqlConnection connection = new NpgsqlConnection(connstring);

            try
            {
                // komenda SQL
                cmd.CommandText = "SELECT language AS \"Language\", FLOOR(population) AS \"Population Use\", ROUND(CAST(percentage as numeric), 2) AS \"Global Population % Use\" " +
                    "FROM countryLanguage JOIN country ON countryLanguage.countrycode = country.code ORDER BY population DESC, language ASC";

                cmd.Connection = connection;
                da = new NpgsqlDataAdapter(cmd);
                da.Fill(ds);
                connection.Open();

                // Uzupełnienie tabeli
                GridView1.DataSource = ds;
                GridView1.DataBind();

                connection.Close();

            }
            catch (Exception)
            {
                connection.Close();
            }
        }
    }
}