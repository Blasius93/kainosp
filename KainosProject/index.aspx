﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="KainosProject.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Countries and languages</title>
    <link rel="Stylesheet" href="~/style.css" type="text/css" />
    
</head>
<body style="background-color: #B9CBFF">
    <form id="form1" runat="server">
    <div>
        <h1 class="auto-style1">Countries and languages</h1>
    </div>
    <div style="text-align: center; height: 30px;"> 
        <a runat="server" href="/countries" class="auto-style2">Countries</a>
        <a runat="server" href="/groupedCountries" class="auto-style2">Languages by Numbers</a>
    </div>
    <div>
        <h3 class="auto-style3">Author: Błażej Galiński</h3>
    </div>
    </form>
</body>
</html>
